import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, Alert } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { COLORS } from '../constants/Colours';

class Home extends Component
{
    constructor(props)
    {
        super(props)
        this.state={
            isButtonDisabled:true,
            editValue:'',
            jsonVal:''
        }
    }

    getData=()=>{
     const url=`https://restcountries.eu/rest/v2/name/${this.state.editValue}`
     fetch(url,{method:'GET'}).then((response)=>{
         if(response.ok)
         {
             response.json().then((json)=>{
                 this.setState({jsonVal:json})
                // console.log(this.state.jsonVal);
                 this.props.navigation.navigate('Countries',{jsonValue:this.state.jsonVal})
             }).catch((err)=>{
                 console.log(err);
                 
             })
         }
         else{
             Alert.alert('Something Went Wrong')
         }
     }).catch((err)=>{
         console.log(err);
         
     });
    }

    render()
    {
        return(
            <View style={styles.container}>
                <TextInput style={styles.editText} value={this.state.editValue} onChangeText={this._onTextChanges}></TextInput>
                <Button color={COLORS.orangr} disabled={this.state.isButtonDisabled} marginTop={border} style={styles.button} onPress={this.getData} title='Submit'></Button>
            </View>
        )
    }
    _onTextChanges=(text)=>{
        if(text.length>0){
            this.setState({isButtonDisabled:false})
        }
        else{
            this.setState({isButtonDisabled:true})
        }
        this.setState({editValue:text})
        console.log(this.state.editValue);
        

    }
}
const border=30;
const styles=StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.white,
        alignItems: 'center',
        justifyContent: 'center',
      },
      editText:{
          borderWidth:2,
          width:'70%',
          borderColor:COLORS.primary
      },
      button:{
          borderColor:COLORS.orangr,
          marginTop:20,
          width:'50%'
      }
});

export default Home