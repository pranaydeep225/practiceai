import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, Alert } from 'react-native';
import { TextInput, FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { COLORS } from '../constants/Colours';

class Weather extends Component
{
    constructor(props)
    {
        super(props)
        this.state={
            capital:'',
            temp:''
        }

    }
componentDidMount()
{
    const{navigation}=this.props;
    const DATA=navigation.getParam('capVal','')
    this.setState({capital:DATA})
    this.getTemperature()
}

getTemperature()
{
    const apikey='14ad5780056eee71f22c7f030b21152e'
    const{navigation}=this.props;
    const DATA=navigation.getParam('capVal','')
    const url=`http://api.weatherstack.com/current?access_key=${apikey}&query=${DATA}`
    console.log(url);
    
    fetch(url,{method:'GET'}).then((response)=>{
        if(response.ok)
        {
            response.json().then((json)=>{
                this.setState({temp:json.current.temperature})
                console.log(this.state.temp);
            }).catch((err)=>{
                console.log(err);
                Alert.alert(err.toString() )  
            })
        }
        else{
            Alert.alert('Something Went Wrong')
        }
    }).catch((err)=>{
        console.log(err);
        Alert.alert(err)
        
    });
}
    render()
    {
        return(<View style={styles.container}>
            <Text>City: {this.state.capital} </Text>
        <Text>Temperature: {this.state.temp}</Text>
        </View>);
    }
}


const styles=StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.white,
         alignItems:'center',
         justifyContent:'center'
      },
});

export default Weather