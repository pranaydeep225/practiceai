import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, Alert } from 'react-native';
import { TextInput, FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { COLORS } from '../constants/Colours';

class Countries extends Component
{
    render()
        {
            const{navigation}=this.props;
       const DATA=navigation.getParam('jsonValue','')
       console.log(DATA);
       
            return(
                <View>
                    <FlatList
                    style={styles.listStyle}
                    data={DATA}
                    renderItem={({item})=>
                <TouchableOpacity style={styles.item} onPress={()=>this._onItemPress(item.capital)}>
                    <View style={{flexDirection:'column'}}>
            <Text>{item.name}</Text>
            <Text>{item.capital}</Text>
                    </View>
                </TouchableOpacity>
                }
                    ></FlatList>
                </View>
            )
        }

        _onItemPress=(capital)=>{
            console.log(capital);
            this.props.navigation.navigate('Weather',{capVal:capital})
            
        }
    
}
const styles=StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.white,
         alignItems:'center',
         justifyContent:'center'
      },
      listStyle:{
          width:'70%',
          alignSelf:'center'
      },

      item:{
          backgroundColor:COLORS.orangr,
          borderColor:COLORS.primary,
          margin:10
      }
    });


export default Countries