import {createAppContainer} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import Home from '../ui/Home'
import Countries from '../ui/Countries';
import Weather from '../ui/Weather';


const screens={
    Home:{
        screen:Home
    },
    Countries:{
        screen:Countries
    },
    Weather:{
        screen:Weather
    }
}
const stack=createStackNavigator(screens);
export default createAppContainer(stack);